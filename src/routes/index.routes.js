const bodyParser = require('body-parser');
const cors = require('cors');

// routes
const user = require('../components/User');
const users = require('../components/Users');

// cors configuration
const corsOptions = require('./config/corsOptions');

module.exports = (app) => {
    const path = '/api/users';

    // middlewares
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use(cors(corsOptions));

    // user routes
    app.use(path, user);

    // users routes
    app.use(path, users);
}