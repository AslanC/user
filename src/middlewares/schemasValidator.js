const Joi = require('joi');
const {userParametersSchema, idParameterSchema} = require('../components/User/model');

async function userValidation(req, res, next) {
    const userData = req.body;
    try {
        await userParametersSchema.validateAsync(userData);
        next();
    } catch (e) {
        console.log(e);
        next('name and email fields they are required');
    }
}

async function idValidation(req, res, next) {
    const {id} = req.params;
    try {
        await idParameterSchema.validateAsync(id);
        next();
    } catch (e) {
        console.log(e);
        next('id is required');
    }
}

module.exports = {
    userValidation,
    idValidation
};