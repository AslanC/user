const { createUser, getUser, updateUser, deleteUser} = require('./service');

async function createUserController(req, res) {
    try {
        const userData = req.body;
        const user = await createUser(userData);
        res.status(200).json(user);
    } catch (error) {
        console.log(error);
        res.status(400).json({message: 'name and email fields they are required'})
    }
}

async function getUserController(req, res) {
    try {
        const id = req.params.id || req.query.id;
        const user = await getUser(id);
        res.status(200).json(user);
    } catch (e) {
        console.log(e);
        res.status(404).json({message: 'User not found'});
    }
}

async function updateUserController(req, res) {
    try {
        let id = req.params.id || req.query.id;
        const data = req.body;
        const userUpdated = await updateUser(id, data);
        res.status(200).json(userUpdated);
    } catch (e) {
        console.log(e);
        res.status(404).json({message: 'User not found'});
    }
}

async function deleteUserController(req, res) {
    try {
        let id = req.params.id || req.query.id;
        const userDeleted = await deleteUser(id);
        const message = {...userDeleted._doc, message: 'user deleted successfully'}
        res.status(200).json(message);
    } catch (e) {
        console.log(e);
        res.status(404).json({message: 'User not found'});
    }
}

module.exports = {
    createUserController,
    getUserController,
    updateUserController,
    deleteUserController
};