const Joi = require('joi');

/**
 * data model valdiator
 */
const userParametersSchema = Joi.object( {
    name: Joi.string()
    .required()
    .min(3),

    email: Joi.string()
    .email()
    .required(),

    profession: Joi.string(),
    edad: Joi.number()
})
.with('name', 'email');

const idParameterSchema = Joi.object( {
    id: Joi.string().required()
});

module.exports = { 
    userParametersSchema,
    idParameterSchema
};