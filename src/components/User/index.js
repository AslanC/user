const express = require('express');

const router = express.Router();

const { createUserController, getUserController, updateUserController,  deleteUserController} = require('./controller');
const { userValidation, idValidation } = require('../../middlewares/schemasValidator');

router.get('/user/', idValidation, getUserController);
router.get('/user/:id', idValidation, getUserController);

router.post('/', userValidation, createUserController);

router.put('/', updateUserController)
router.put('/:id', updateUserController)

router.delete('/', deleteUserController)
router.delete('/:id', deleteUserController)

module.exports = router;