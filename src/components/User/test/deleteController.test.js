const { deleteUserController } = require('../controller');

const mockResponses = () => {
    const res = {};
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    res.send = jest.fn().mockReturnValue(res);
    return res;
};

jest.mock('../service.js');
const { deleteUser } = require('../service');


describe('test controller User', () => {
    const mockGetUser = {
        _id: "6036af32eaf76c3ce47dec56",
        name: "test 3",
        email: "test@test.com",
        profession: "captain",
        edad: 28,
        createdAt: "2021-02-24T19:55:30.298Z",
        updatedAt: "2021-02-24T19:55:30.298Z",
        __v: 0
    }

    test('delete user successful', async (done) => {
        const message = {...mockGetUser, message: 'user deleted successfully'}
        try {
            deleteUser.mockReturnValue(Promise.resolve(message));
        } catch (error) {
            console.log(error);
        }
        const res = mockResponses();
        const req = {
            params: {
                id: '6036af32eaf76c3ce47dec56',
            },
        };

        await deleteUserController(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBe(message);
        done();
    });
})