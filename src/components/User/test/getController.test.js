const { getUserController } = require('../controller');
const { getUser } = require('../service');

const mockResponses = () => {
    const res = {};
    res.status = jest.fn().mockReturnValue(res);
    res.json = jest.fn().mockReturnValue(res);
    res.send = jest.fn().mockReturnValue(res);
    return res;
};

jest.mock('../service.js');

describe('test controller User', () => {
    const mockGetUser = {
        _id: "60365bf5dae9030848ee977a",
        name: "test",
        email: "test@test.com",
        profession: "captain",
        edad: 28,
    }

    test('get user successful', async (done) => {
        getUser.mockReturnValue(Promise.resolve(mockGetUser));
        const res = mockResponses();
        const req = {
            params: {
                id: '60365bf5dae9030848ee977a',
            },
        };
        await getUserController(req, res);
        expect(res.status).toBeCalledWith(200);
        expect(res.json).toBeCalledWith(mockGetUser);
        done();
    });

    test('get user failure', async (done) => {
        getUser.mockReturnValue(Promise.resolve(mockGetUser));
        const res = mockResponses();
        const req = {
            params: {
                id: '',
            },
        };
        await getUserController(req, res);
        expect(res.status).toBeCalledWith(404);
        expect(res.json).toBeCalledWith({message: 'User not found'});
        done();
    });
})