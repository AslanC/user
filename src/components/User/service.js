const User = require('../../common/models/users');

const createUser = async (user) => await User.create(user);
const getUser = async (id) => await User.findById(id);
const updateUser = async (id, data) => await User.findByIdAndUpdate(id, data, {new:true});
const deleteUser = async (id) => await User.findByIdAndRemove(id);

module.exports = { 
    createUser,
    getUser,
    updateUser,
    deleteUser
};