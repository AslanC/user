const { getUsers } = require('./service');

async function getUsersController(req, res) {
    try {
        const users = await getUsers();
        res.status(200).json(users);
    } catch (e) {
        res.status(404).json({message: `No users currently`})
    }
}

module.exports = {
    getUsersController
};