const User = require('../../common/models/users');

const getUsers = async () => User.find({});

module.exports = {
    getUsers
};