const express = require('express');

const router = express.Router();

const { getUsersController } = require('./controller');

router.get('/', getUsersController);

module.exports = router;