const mongoose = require('mongoose');
require('dotenv').config();

const { DB_CONNECTION } = process.env;

async function connection() {
    try {
        await mongoose.connect(DB_CONNECTION,
            {
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false
            });
        return 'Successful connection to database';
    } catch(e) {
        const message = `connection error \n${e}`
        return message;
    }
}

module.exports = connection;