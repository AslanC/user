require('dotenv').config();
const express = require('express');

const connection = require('./src/common/database');

const app = express();
const { PORT, HOST } = process.env;

require('./src/routes/index.routes')(app);

const server = app.listen(PORT, async() => {
    console.info(await connection());
    console.info(`${HOST}:${PORT}`);
});

module.exports = server;
